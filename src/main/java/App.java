import controller.BookingController;
import controller.FlightController;

import java.io.*;
import java.util.*;

public class App {
    static BookingController bookingController = new BookingController();
    static FlightController flightController = new FlightController();

    static void printConsole() {
        System.out.println(
            "\n Enter number of interesting option: \n" +
            "1. Online table: \n" + 
            "2. Check flight info: \n" +
            "3. Search and check flight: \n" +
            "4. Cancel booking: \n" +
            "5. My flights: \n" +
            "6. Exit console"
        );
    }

    static void copyFlights() {
        try {
            Scanner scanner = new Scanner(new FileReader("target/classes/database/Flight.txt"));
            flightController.genFlights(scanner);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    static void copyBookings() {
        try {
            Scanner scanner = new Scanner(new FileReader("target/classes/database/Booking.txt"));
            bookingController.getBookingsFromFile(scanner);
        } catch (InputMismatchException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]) {
        printConsole();
        copyBookings();
        copyFlights();

        Scanner scanner = new Scanner(System.in);
        String userName = null;
        String userSurname = null;
        int userSelection = scanner.nextInt();

        while (userSelection != 6) {
            switch (userSelection) {
                case 1:
                    flightController.showAllFlights();
                    break;
                case 2:
                    System.out.println("Enter flight id: ");
                    Scanner scanner2 = new Scanner(System.in);
                    String userSelectFlightID = scanner2.nextLine();
                    System.out.println(flightController.getFlightByID(userSelectFlightID));
                    break;
                case 3:
                    System.out.println("Enter destination of flight: ");
                    Scanner scanner3 = new Scanner(System.in);
                    String userDestinationSelect = scanner3.nextLine();

                    System.out.println("Enter date of take off: ");
                    scanner3 = new Scanner(System.in);
                    String userSelectFlight = scanner3.nextLine();

                    System.out.println("Enter number of passengers: ");
                    scanner3 = new Scanner(System.in);
                    int userPassengerNumSelect = scanner3.nextInt();

                    flightController.showReqFlights(userSelectFlight, userDestinationSelect, userPassengerNumSelect);

                    System.out.println("Enter number of flight you want to add in your booking: ");
                    scanner3 = new Scanner(System.in);
                    try {
                        int userBookingSelect = scanner3.nextInt();
                        break;
                    } catch (Exception e) {
                        //TODO: handle exception
                    }

                    String userFlightID = scanner3.nextLine();
                    for (int i = 0; i < userPassengerNumSelect; i++) {
                        System.out.printf("Enter name for passenger number %s: ", i + 1);
                        scanner3 = new Scanner(System.in);
                        userName = scanner3.nextLine();

                        System.out.printf("Enter surname for passenger number %s: ", i + 1);
                        scanner3 = new Scanner(System.in);
                        userSurname = scanner3.nextLine();

                        bookingController.createBooking(userFlightID, userName, userSurname);
                    }
                    break;
                case 4:
                    System.out.print("Enter your booking ID you want to suspend: ");
                    Scanner scanner4 = new Scanner(System.in);
                    int userBookingIDDel = scanner4.nextInt();
                    bookingController.delBooking(userBookingIDDel);
                    break;
                case 5:
                    System.out.print("Enter your name: ");
                    Scanner scanner5 = new Scanner(System.in);
                    userName = scanner5.nextLine();

                    System.out.print("Enter your surname: ");
                    userSurname = scanner5.nextLine();

                    bookingController.showUsersBookings(userName, userSurname, flightController);
                    break;
                default:
                    System.out.println("User didn`t make a decision");
            }
            printConsole();
            userSelection = scanner.nextInt();
            if (userSelection == 6) {
                bookingController.downloadBookingToFile(userName, userSurname, flightController);
            }
        }
    }
}