package dao;

import java.util.*;
import entity.Booking;

public interface BookingDao {
    void createBooking(String flightID, String passengerName, String passengerSurname);
    List<Booking> getAll();
    void getBookingsFromFile(Scanner scanner);
    void updBooking(Booking updatedBooking);
    boolean delBooking(int BookingID);
}
