package dao;

import java.util.*;
import entity.Booking;

public class BookingDaoImpl implements BookingDao {
    private List<Booking> listBooking = new ArrayList<>();
    private Scanner innerScanner;

    @Override
    public void createBooking(String flightID, String passengerName, String passengerSurname) {
        Booking booking = new Booking(flightID, passengerName, passengerSurname);
        if (listBooking.isEmpty()) {
            booking.setBookingID(1);
        } else {
            int lastBookingIndex = listBooking.size() - 1;
            int newBookingId = listBooking.get(lastBookingIndex).getBookingID() + 1;
            booking.setBookingID(newBookingId);
        }
        listBooking.add(booking);
        System.out.println(booking);
    }

    @Override
    public List<Booking> getAll() {
        return listBooking;
    }

    @Override
    public void getBookingsFromFile(Scanner scanner) {
        String line;
        while (scanner.hasNextLine()) {
            line = scanner.nextLine();
            if (line.equals("--------------------------------------------------------------------------------")) {
                continue;
            }
            innerScanner = new Scanner(line);
            innerScanner.useDelimiter("\t");
            int bookingId = innerScanner.nextInt();
            String name = innerScanner.next();
            String surname = innerScanner.next();
            String flightId = innerScanner.next();
            Booking booking = new Booking(bookingId, flightId, name, surname);

            listBooking.add(booking);
        }
    }

    @Override
    public void updBooking(Booking updatedBooking) {
        if (listBooking.contains(updatedBooking)) {
            listBooking.set(listBooking.indexOf(updatedBooking), updatedBooking);
        } else listBooking.add(updatedBooking);
    }

    @Override
    public boolean delBooking(int bookingID) {
        for (Booking booking : listBooking) {
            if (booking.getBookingID() == bookingID) {
                System.out.println(booking);
                listBooking.remove(booking);
                System.out.println("Your booking was canceled");
                return true;
            }
        }
        System.out.println("Your booking ID isn`t exist");
        return false;
    }
}