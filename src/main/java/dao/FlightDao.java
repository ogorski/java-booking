package dao;

import entity.Flight;
import java.util.*;

public interface FlightDao {
    void createFlight(String flightID, String destination, String departureTime, int emptySeats);
    List<Flight> getAll();
    void genFlights(Scanner scanner);
    Flight getFlightByID(String id);
    boolean delFlight(String flightToDeleteID);
    boolean delFlight(Flight flightToDelete);
}
