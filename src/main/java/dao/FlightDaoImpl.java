package dao;

import java.util.*;
import entity.Flight;

public class FlightDaoImpl implements FlightDao {
    private List<Flight> listFlight = new ArrayList<>();

    @Override
    public List<Flight> getAll() {
        return listFlight;
    }

    @Override
    public void createFlight(String flightID, String destination, String departureTime, int emptySeats) {
        Flight flight = new Flight(flightID, destination, departureTime, emptySeats);
        listFlight.add(flight);
    }

    @Override
    public void genFlights(Scanner scanner) {
        while (scanner.hasNext()) {
            String flightID = scanner.nextLine();
            String destination = scanner.nextLine();
            String departureTime = scanner.nextLine();
            int emptySeats = Integer.parseInt(scanner.nextLine());
            Flight flight = new Flight(flightID, destination, departureTime, emptySeats);
            listFlight.add(flight);
        }

    }

    @Override
    public Flight getFlightByID(String id) {
        for (Flight flight : listFlight) {
            if (flight.getFlightID().equals(id)) {
                return flight;
            }
        }
        return null;
    }

    @Override
    public boolean delFlight(String flightToDeleteID) {
        for (Flight flight : listFlight) {
            if (flight.getFlightID().equals(flightToDeleteID)) {
                listFlight.remove(flightToDeleteID);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean delFlight(Flight flightToDelete) {
        for (Flight flight : listFlight) {
            if (flight.equals(flightToDelete)) {
                listFlight.remove(flightToDelete);
                return true;
            }
        }
        return false;
    }
}
