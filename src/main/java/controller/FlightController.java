package controller;

import java.util.*;

import service.FlightService;
import entity.Flight;

public class FlightController {
    private FlightService flightService = new FlightService();

    public void createFlight(String flightID, String destination, String departureTime, int emptySeats) {
        flightService.createNewFlight(flightID, destination, departureTime, emptySeats);
    }

    public void genFlights(Scanner scanner) {
        flightService.genFlights(scanner);
    }

    public List<Flight> getFlights() {
        return flightService.getAllFlights();
    }

    public Flight getFlightByID(String id) {
        return flightService.getFlightByID(id);
    }

    public boolean delFlightByID(String id) {
        return flightService.delFlightByID(id);
    }

    public boolean delFlight(Flight flight) {
        return flightService.delFlight(flight);
    }

    public void showAllFlights() {
        flightService.showAllFlights();
    }

    public List<Flight> reqFlights(String destination, String departureTime, int passenger_Num) {
        return flightService.reqFlights(destination, departureTime, passenger_Num);
    }

    public void showReqFlights(String destination, String departureTime, int passenger_Num) {
        flightService.showReqFlights(destination, departureTime, passenger_Num);    
    }
}
