package controller;

import java.util.List;
import java.util.Scanner;

import entity.Booking;
import service.BookingService;
import service.FlightService;

public class BookingController {
    private BookingService bookingService = new BookingService();
    private FlightService flightService = new FlightService();

    public void createBooking(String flightID, String passengerName, String passengerSurname) {
        bookingService.createBooking(flightID, passengerName, passengerSurname);
    }

    public List<Booking> getBookings() {
        return bookingService.getBookings();
    }

    public void getBookingsFromFile(Scanner scanner) {
        bookingService.getBookingsFromFile(scanner);
    }

    public void updBooking(Booking updatedBooking) {
        bookingService.updBooking(updatedBooking);
    }

    public boolean delBooking(int bookingID) {
        return bookingService.delBooking(bookingID);
    }

    List<Booking> getUsersBookings(String passengerName, String passengerSurname) {
        return bookingService.getUserBookings(passengerName, passengerSurname);
    }

    public void showUsersBookings(String passengerName, String passengerSurname, FlightController flightController) {
        bookingService.showUsersBookings(passengerName, passengerSurname, flightController);
    }

    public void downloadBookingToFile(String passengerName, String passengerSurname, FlightController flightController) {
        bookingService.downloadBookingToFile(passengerName, passengerSurname, flightController);
    }
}
