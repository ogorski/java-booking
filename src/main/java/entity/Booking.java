package entity;

import java.util.*;

public class Booking {
    private int bookingID;
    private String flightID;
    private String passengerName;
    private String passengerSurname;
    private boolean isBookingValid;

    public Booking(String flightID, String passengerName, String passengerSurname) {
        this.flightID = flightID;
        this.passengerName = passengerName;
        this.passengerSurname = passengerSurname;
        isBookingValid = true;
    }

    public Booking(int bookingID, String flightID, String passengerName, String passengerSurname) {
        this.bookingID = bookingID;
        this.flightID = flightID;
        this.passengerName = passengerName;
        this.passengerSurname = passengerSurname;
        isBookingValid = true;
    }

    public String getFlightID() {
        return flightID;
    }

    public void setFlightID(String flightID) {
        this.flightID = flightID;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public String getPassengerSurname() {
        return passengerSurname;
    }

    public void setPassengerSurname(String passengerSurname) {
        this.passengerSurname = passengerSurname;
    }

    public int getBookingID() {
        return bookingID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public boolean isBookingValid() {
        return isBookingValid;
    }

    public void setBookingValid(boolean bookingValid) {
        isBookingValid = bookingValid;
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookingID, flightID, passengerName, passengerSurname, isBookingValid);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Booking booking = (Booking) o;
        return bookingID == booking.bookingID &&
               isBookingValid == booking.isBookingValid &&
               Objects.equals(flightID, booking.flightID) &&
               Objects.equals(passengerName, booking.passengerSurname) &&
               Objects.equals(passengerSurname, booking.passengerName);
    }
    @Override
    public String toString() {
        return "Booking ID: " + bookingID +
                "\nFull name: " + passengerName + " " + passengerSurname +
                "\nFlight ID: " + flightID;
    }
}
