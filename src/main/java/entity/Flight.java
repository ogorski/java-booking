package entity;

import java.text.*;
import java.util.*;

public class Flight {
    private String flightID;
    private String destination;
    private Date departureTime;
    private int emptySeats;

    public Flight(String flightID, String destination, String departureTime, int emptySeats) {
        this.flightID = flightID;
        this.destination = destination;

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        try {
            this.departureTime = dateFormat.parse(departureTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        this.emptySeats = emptySeats;
    }

    public String getFlightID() {
        return flightID;
    }

    public void setFlightID(String flightID) {
        this.flightID = flightID;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Date getDepartureDate() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public int getEmptySeats() {
        return emptySeats;
    }

    public void setEmptySeats(int emptySeats) {
        this.emptySeats = emptySeats;
    }

    @Override
    public int hashCode() {
        return Objects.hash(flightID, destination, departureTime);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return flightID.equals(flight.flightID) &&
               destination.equals(flight.destination) &&
               departureTime.equals(flight.departureTime);
    }
    @Override
    public String toString() {
        return flightID + "\t" +
               destination + "\t" +
               new SimpleDateFormat("dd/MM/yyyy HH:mm").format(departureTime) + "\t" +
               emptySeats + "free seats" + "\n" +
               "------------------------------------";
    }
    public String toShortString() {
        return "\nDestination of flight: " + destination +
                "\nDate of take off: " + new SimpleDateFormat("dd/MM/yyyy HH:mm").format(departureTime);
    }
}
