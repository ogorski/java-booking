package service;

import java.text.*;
import java.util.*;
import java.util.stream.Collectors;

import entity.Flight;

import dao.FlightDao;
import dao.FlightDaoImpl;

public class FlightService {
    private FlightDao flightDao = new FlightDaoImpl();

    public void createNewFlight(String flightID, String destination, String departureTime, int emptySeats) {
        flightDao.createFlight(flightID, destination, departureTime, emptySeats);
    }

    public void genFlights(Scanner scanner) {
        flightDao.genFlights(scanner);
    }

    public List<Flight> getAllFlights() {
        return flightDao.getAll();
    }

    public boolean delFlight(Flight flight) {
        return flightDao.delFlight(flight);
    }

    public Flight getFlightByID(String id) {
        return flightDao.getFlightByID(id);
    }

    public boolean delFlightByID(String id) {
        return flightDao.delFlight(id);
    }

    public void showAllFlights() {
        getAllFlights().stream().forEach(System.out::println);
    }

    public List<Flight> reqFlights(String destination, String departureDate, int passengerNum) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return getAllFlights()
               .stream()
               .filter(f -> f.getDestination().equals(destination)
                        && dateFormat.format(f.getDepartureDate()).substring(0, 10).equals(departureDate)
                        && f.getEmptySeats() >= passengerNum)
                .collect(Collectors.toList());
    }

    public void showReqFlights(String destination, String departureDate, int passengerNum) {
        reqFlights(destination, departureDate, passengerNum).stream().forEach(System.out::println);
    }
}
