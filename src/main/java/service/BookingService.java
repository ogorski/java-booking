package service;

import controller.FlightController;
import dao.BookingDao;
import dao.BookingDaoImpl;
import entity.Booking;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;

public class BookingService {
    private BookingDao bookingDao = new BookingDaoImpl();

    public void createBooking(String flightID, String passengerName, String passengerSurname) {
        bookingDao.createBooking(flightID, passengerName, passengerSurname);
    }

	public List<Booking> getBookings() {
		return bookingDao.getAll();
	}

	public void getBookingsFromFile(Scanner scanner) {
        bookingDao.getBookingsFromFile(scanner);
	}

	public void updBooking(Booking updatedBooking) {
        bookingDao.updBooking(updatedBooking);
	}

	public boolean delBooking(int bookingID) {
		return bookingDao.delBooking(bookingID);
	}

	public List<Booking> getUserBookings(String passengerName, String passengerSurname) {
		return bookingDao.getAll().stream().filter(b -> b.getPassengerName().equals(passengerName) && b.getPassengerSurname().equals(passengerSurname)).collect(Collectors.toList());
	}

	public void showUsersBookings(String passengerName, String passengerSurname, FlightController flightController) {
        getUserBookings(passengerName, passengerSurname).stream()
            .forEach(booking -> {
                System.out.print(booking.toString());
                System.out.print(flightController.getFlightByID(booking.getFlightID()).toShortString() + "\n");
                System.out.print(booking.isBookingValid() ? "Booking status: VALID" : "Booking status: CANCELLED");
                System.out.println("\n------------------------------------------------------------------------------------");
            });
	}

	public void downloadBookingToFile(String passengerName, String passengerSurname, FlightController flightController) {
        Path path = Paths.get("database/Booking.txt");
        try {
            Files.write(path, "".getBytes());
        } catch (IOException e) {
            System.out.println("Creation file to fill failed");
        }

        getBookings()
            .stream()
            .forEach(booking -> {
            try {
                Files.write(path, booking.toString()
                        .getBytes(), StandardOpenOption.APPEND);
                Files.write(path, (flightController.getFlightByID(booking.getFlightID()).toShortString() + "\t")
                        .getBytes(), StandardOpenOption.APPEND);
                Files.write(path, (booking.isBookingValid() ? "booking status: VALID" : "booking status: CANCELLED")
                        .getBytes(), StandardOpenOption.APPEND);
                Files.write(path, ("\n" + "--------------------------------------------------------------------------------" + "\n")
                        .getBytes(), StandardOpenOption.APPEND);
            } catch (IOException e) {
                System.out.println("Fill failed");
            }
        });
	}
}
